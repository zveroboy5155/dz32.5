﻿

#include "DZ32.5.h"

void seachByActor(nlohmann::json dict, std::string &actor){
    std::string cur_name;

    for(auto it = dict.begin();it != dict.end(); ++it) {
        auto cur_actors = it->find("role");
        for (auto role = cur_actors->begin(); role != cur_actors->end(); ++role) {
            cur_name = role.value();
            if(cur_name.find(actor) != std::string::npos) {
                std::cout <<"In film " << it.key() + " actor " + cur_name + " playing " + role.key() << std::endl;
            }
        }
    }
}

void task2() {
	std::ifstream file("../task2/record.json");
	//setlocale(0, "");
	nlohmann::json dict;
	std::string str;

	if (!file) {
		std::cout << "Data Base is empty";
		file.close();
		return;
	}
	file >> dict;
start:
	std::cout << "Enter 1st, 2nd or Full Name of actor fo search" << std::endl;
	std::cin.clear();
	if (getline(std::cin, str) && !str.empty()) seachByActor(dict, str);
	else goto start;
	file.close();
}
