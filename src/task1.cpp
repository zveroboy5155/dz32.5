#include "DZ32.5.h"

std::vector <std::string> film_info{"name", "country", "date",
								 "director", "screenwriter", "producer", "tagline", "role"};
std::vector <std::string> actor_info{ "hero name", "actor name" };


std::string add_dict(nlohmann::json &dict, bool repeatFl = false, std::vector <std::string> &cur_fields = film_info) {
	std::string value, key;
	nlohmann::json actors;

	/*if (!repeatFl){
		std::cout << "Enter value of flied " << cur_fields[0] << ":";
		std::cin >> key;
	}*/

	do {
		for (int i = 0; i < cur_fields.size(); i++) {
			if (cur_fields[i] == "role") {
				add_dict(actors, true, actor_info);
				dict["role"] = actors;
				//for (auto j = 0; j < 4; j++) add_dict(new_film, fields.size() - 1);
				break;
			}
			
			std::cout << "Enter value of flied " << cur_fields[i] << ":";
			std::cin >> value;
			if (!repeatFl) { 
				dict[cur_fields[i]] = value; 
				continue;
			}
			if(key.empty()) key = value;
			else {
				dict[key] = value;
				key.clear();
			}
		}
		if (repeatFl) { 
			std::cout << "Add new record Yes/No:";
			std::cin >> value;
			if(value == "No") repeatFl = false;
		}
	} while (repeatFl);
	return key;
}

void task1() {
	nlohmann::json dict;
	std::ofstream file;
	file.open("../task1/rec.json");

	add_dict(dict);
	//std::cout << "Hell task1" << std::endl;

	//std::cout << dict << std::endl;
	if (file) file << dict;
	else { 
		std::cout << "File not opened" << std::endl;
		std::cout << dict << std::endl;
	}
	file.close();
}